import { Dispatch, SetStateAction, useState } from "react"
import { ExamType, ProcedureOrExam } from "../components/ExamForm/Layout/interfaces"
import { PacientInfo } from "../reducers/Auth/AuthInterfaces"
import axios from 'axios'
import { API_URL } from "../utils/api_config"

interface useExameFormOutput {
    title: string
    setTitle: Dispatch<SetStateAction<string>>
    description: string
    setDescription: Dispatch<SetStateAction<string>>
    is_procedure: ProcedureOrExam
    setIs_procedure: Dispatch<SetStateAction<ProcedureOrExam>>
    selectedPacient: number | null
    setSelectedPacient: Dispatch<SetStateAction<number | null>>
    handleProcedureOrExameLabel: (value: ProcedureOrExam) => string
    formatProcedureOrExameLabel: (value: string) => ProcedureOrExam
    registerExam: (data: ExamType, callback: (exam: ExamType) => void) => Promise<void>
}

const useExamForm = (): useExameFormOutput => {
    const [title, setTitle] = useState<string>('')
    const [description, setDescription] = useState<string>('')
    const [is_procedure, setIs_procedure] = useState<ProcedureOrExam>('exam')
    const [selectedPacient, setSelectedPacient] = useState<number | null>(null)

    const handleProcedureOrExameLabel = (value: ProcedureOrExam): string => {
        switch (value) {
            case 'exam':
                return 'Exame'
            case 'procedure':
                return 'Procedimento'
            default:
                return ''
        }
    }

    const formatProcedureOrExameLabel = (value: string): ProcedureOrExam => {
        switch (value) {
            case 'Exame':
                return 'exam'
            case 'Procedimento':
                return 'procedure'
            default:
                return ''
        }
    }

    const registerExam = async (data: ExamType, callback: (exam: ExamType) => void): Promise<void> => {
        try {
            const result = await axios.post(`${API_URL}/exam/`, data)
            if(result.status === 201){
                const exam_result: ExamType = result.data.data
                callback(exam_result)
            }
        }catch(err){
            console.log('ERROR REGISTERING EXAM ==>', err)
            return
        }
    }

    return {
        //State
        title,
        setTitle,
        description,
        setDescription,
        is_procedure,
        setIs_procedure,
        selectedPacient,
        setSelectedPacient,
        handleProcedureOrExameLabel,
        formatProcedureOrExameLabel,
        registerExam
    }
}

export default useExamForm