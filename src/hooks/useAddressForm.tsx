import { AddressType } from "../reducers/Auth/AuthInterfaces"
import axios from 'axios'
import { API_URL } from "../utils/api_config"
import { Dispatch, SetStateAction, useState } from "react"

interface useAddressFormOutput {
    //Functions
    registerAddress: () => Promise<AddressType | null>
    //State
    street_name: string
    setStree_name: Dispatch<SetStateAction<string>>
    zipcode: string
    setZipcode: Dispatch<SetStateAction<string>>
    number: string
    setNumber: Dispatch<SetStateAction<string>>
    city: string
    setCity: Dispatch<SetStateAction<string>>
    state: string
    setState: Dispatch<SetStateAction<string>>
    country: string
    setCountry: Dispatch<SetStateAction<string>>
}

const useAddressForm = (): useAddressFormOutput => {
    const [street_name, setStree_name] = useState<string>('')
    const [zipcode, setZipcode] = useState<string>('')
    const [number, setNumber] = useState<string>('')
    const [city, setCity] = useState<string>('')
    const [state, setState] = useState<string>('')
    const [country, setCountry] = useState<string>('')

    const registerAddress = async (): Promise<AddressType | null> => {
        try {
            const data : AddressType = {
                street_name,
                zipcode,
                number,
                city,
                state,
                country
            }
            const result = await axios.post(`${API_URL}/address/`, data)
            if(result.status === 201) {
                const address: AddressType = result.data.data
                return address
            }
            return null
        } catch (err) {
            console.log('ERROR REGISTERING ADDRESS =>', err)
            return null
        }
    }

    return {
        //Functions
        registerAddress,
        //State
        street_name,
        setStree_name,
        zipcode,
        setZipcode,
        number,
        setNumber,
        city,
        setCity,
        state,
        setState,
        country,
        setCountry
    }
}

export default useAddressForm
