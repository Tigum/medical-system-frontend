import { AddressType, PacientInfo, UserInfo } from '../reducers//Auth/AuthInterfaces'
import axios from 'axios'
import { API_URL } from "../utils/api_config"
import { ServiceStationInfo, StationTypes } from '../reducers/Forms/FormsInterfaces'
import { ExamType } from '../components/ExamForm/Layout/interfaces'

interface useQueryOutput {
    fetchUsers: () => Promise<UserInfo[]>
    fetchUser: (user_id: number) => Promise<UserInfo | null>
    handleRole: (role: string) => string
    fetchPacients: () => Promise<PacientInfo[]>
    fetchPacient: (pacient_id: number | string) => Promise<PacientInfo | null>
    fetchPlaces: () => Promise<ServiceStationInfo[]>
    fetchExams: () => Promise<ExamType[]>
    fetchAddress: (address_id: number | string) => Promise<AddressType | null>
    handleStationType: (station_type: string) => StationTypes
    formatStationType: (station_type: StationTypes) => string
    removeStation: (station_id: number, callback: (places: ServiceStationInfo[]) => void) => Promise<void>
    removeUser: (user_id: number, callback: (users: UserInfo[]) => void) => Promise<void>
    removeExam: (exam_id: number, callback: (exams: ExamType[]) => void) => Promise<void>
    removePacient: (pacient_id: number, callback: (pacients: PacientInfo[]) => void) => Promise<void>
}

const useQuery = (): useQueryOutput => {
    const fetchUsers = async (): Promise<UserInfo[]> => {
        try {
            const result = await axios.get(`${API_URL}/user/`)
            if (result.status === 200) {
                return result.data.data.sort((a: UserInfo, b: UserInfo) => a.name < b.name ? -1 : 1)
            }
            return []
        } catch (err) {
            console.log('ERROR FETCHING USERS ==> ', err)
            return []
        }
    }

    const fetchUser = async (user_id: number): Promise<UserInfo | null> => {
        try {
            const result = await axios.get(`${API_URL}/user/${user_id}`)
            if (result.status === 200) {
                return result.data.data
            }
            return null
        } catch (err) {
            console.log('ERROR FETCHING USER ==> ', err)
            return null
        }
    }

    const fetchPacients = async (): Promise<PacientInfo[]> => {
        try {
            const result = await axios.get(`${API_URL}/pacient/`)
            if (result.status === 200) {
                return result.data.data.sort((a: PacientInfo, b: PacientInfo) => a.name < b.name ? -1 : 1)
            }
            return []
        } catch (err) {
            console.log('ERROR FETCHING PACIENTS ==> ', err)
            return []
        }
    }

    const fetchPacient = async (pacient_id: number | string): Promise<PacientInfo | null> => {
        try {
            const result = await axios.get(`${API_URL}/pacient/${pacient_id}`)
            if (result.status === 200) {
                return result.data.data
            }
            return null
        } catch (err) {
            console.log('ERROR FETCHING PACIENT ==> ', err)
            return null
        }
    }

    const fetchPlaces = async (): Promise<ServiceStationInfo[]> => {
        try {
            const result = await axios.get(`${API_URL}/service_station/`)
            if (result.status === 200) {
                return result.data.data.sort((a: ServiceStationInfo, b: ServiceStationInfo) => a.label < b.label ? -1 : 1)
            }
            return []
        } catch (err) {
            console.log('ERROR FETCHING PLACES ==> ', err)
            return []
        }
    }

    const fetchExams = async (): Promise<ExamType[]> => {
        try {
            const result = await axios.get(`${API_URL}/exam/`)
            if (result.status === 200) {
                return result.data.data.sort((a: ExamType, b: ExamType) => a.title < b.title ? -1 : 1)
            }
            return []
        } catch (err) {
            console.log('ERROR FETCHING EXAMS ==> ', err)
            return []
        }
    }

    const fetchAddress = async (address_id: number | string): Promise<AddressType | null> => {
        try {
            const result = await axios.get(`${API_URL}/address/${address_id}`)
            if (result.status === 200) {
                return result.data.data
            }
            return null
        } catch (err) {
            console.log('ERROR FETCHING ADDRESS ==> ', err)
            return null
        }
    }

    const removeStation = async (station_id: number, callback: (places: ServiceStationInfo[]) => void): Promise<void> => {
        try {
            const result = await axios.delete(`${API_URL}/service_station/${station_id}`)
            if (result.status === 204) {
                const stations: ServiceStationInfo[]= await fetchPlaces()
                callback(stations)
                return result.data.data
            }
            return 
        } catch (err) {
            console.log('ERROR DELETING PLACE ==> ', err)
            return
        }
    }

    const removeUser = async (user_id: number, callback: (users: UserInfo[]) => void): Promise<void> => {
        try {
            const result = await axios.delete(`${API_URL}/user/${user_id}`)
            if (result.status === 204) {
                const users: UserInfo[]= await fetchUsers()
                callback(users)
                return result.data.data
            }
            return 
        } catch (err) {
            console.log('ERROR DELETING USER ==> ', err)
            return
        }
    }

    const removeExam = async (exam_id: number, callback: (exams: ExamType[]) => void): Promise<void> => {
        try {
            const result = await axios.delete(`${API_URL}/exam/${exam_id}`)
            if (result.status === 204) {
                const exams: ExamType[]= await fetchExams()
                callback(exams)
                return result.data.data
            }
            return 
        } catch (err) {
            console.log('ERROR DELETING USER ==> ', err)
            return
        }
    }

    const removePacient = async (pacient_id: number, callback: (pacients: PacientInfo[]) => void): Promise<void> => {
        try {
            const result = await axios.delete(`${API_URL}/pacient/${pacient_id}`)
            if (result.status === 204) {
                const pacients: PacientInfo[]= await fetchPacients()
                callback(pacients)
                return result.data.data
            }
            return 
        } catch (err) {
            console.log('ERROR DELETING PACIENT ==> ', err)
            return
        }
    }

    const handleRole = (role: string): string => {
        switch (role) {
            case 'admin':
                return 'Administrador'
            case 'doctor':
                return 'Médico'
            case 'master':
                return 'Administrador Master'
            case 'pacient':
                return 'Paciente'
            default:
                return ''
        }
    }

    const handleStationType = (station_type: string): StationTypes => {
        switch (station_type) {
            case 'Hospital':
                return 'hospital'
            case 'Laboratório':
                return 'lab'
            case 'Clínica':
                return 'clinic'
            case 'Posto de Saúde':
                return 'health_station'
            default:
                return ''
        }
    }

    const formatStationType = (station_type: StationTypes): string => {
        switch (station_type) {
            case 'hospital':
                return 'Hospital'
            case 'lab':
                return 'Laboratório'
            case 'clinic':
                return 'Clínica'
            case 'health_station':
                return 'Posto de Saúde'
            default:
                return ''
        }
    }

    return {
        fetchUsers,
        fetchUser,
        handleRole,
        fetchPacients,
        fetchPacient,
        fetchPlaces,
        fetchAddress,
        handleStationType,
        formatStationType,
        removeStation,
        removeUser,
        removeExam,
        removePacient,
        fetchExams
    }
}

export default useQuery