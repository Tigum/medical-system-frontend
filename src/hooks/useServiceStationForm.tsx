import { Dispatch, SetStateAction, useState } from "react"
import { StationTypes } from "../reducers/Forms/FormsInterfaces"

interface useServiceStationFormOutput {
    resetState: () => void
    label: string
    setLabel: Dispatch<SetStateAction<string>>
    isPrivate: string
    setIsPrivate: Dispatch<SetStateAction<string>>
    station_type: StationTypes
    setStation_type: Dispatch<SetStateAction<StationTypes>>
}

const useServiceStationForm = (): useServiceStationFormOutput => {
    const [label, setLabel] = useState<string>('')
    const [isPrivate, setIsPrivate] = useState<string>('')
    const [station_type, setStation_type] = useState<StationTypes>('hospital')

    const resetState = (): void => {
        setLabel('')
        setIsPrivate('')
        setStation_type('hospital')
    }

    return {
        //Functions
        resetState,

        //State
        label,
        setLabel,
        isPrivate,
        setIsPrivate,
        station_type,
        setStation_type
    }
}

export default useServiceStationForm