import axios from 'axios'
import qs from 'querystring'
import { TOKEN_KEY } from '../utils/token_config'

const useApiInterceptors = () => {
    const ConfigApiRequests = () => axios.interceptors.request.use((config) => {
        config.headers = {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Cache-Control': 'no-cache',
        }

        try {
            const token: string | null = localStorage.getItem(TOKEN_KEY)

            if (token) {
                config.headers['Authorization'] = `${token}`
                config.paramsSerializer = (params: any) => {
                    return qs.stringify(params)
                }
            }
            return config

        } catch (err) {
            console.log('CHECK TOKEN ERROR ==> ', err)
            return config
        }

    }, (err: any) => console.log('INTERCEPTOR REQUEST ERROR ->', err))

    return {
        ConfigApiRequests
    }

}

export default useApiInterceptors
