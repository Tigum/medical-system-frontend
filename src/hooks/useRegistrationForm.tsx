import { Dispatch, SetStateAction, useState } from "react"

interface useRegistrationFormOutput {
    resetState: () => void
    name: string
    setName: Dispatch<SetStateAction<string>>
    surname: string
    setSurname: Dispatch<SetStateAction<string>>
    phone: string
    setPhone: Dispatch<SetStateAction<string>>
    email: string
    setEmail: Dispatch<SetStateAction<string>>
    password: string
    setPassword: Dispatch<SetStateAction<string>>
    confirmPassword: string
    setConfirmPassword: Dispatch<SetStateAction<string>>
    role: 'master' | 'admin' | 'doctor' | 'pacient' | ''
    setRole: Dispatch<SetStateAction<'master' | 'admin' | 'doctor' | 'pacient' | ''>>
    start_at: string
    setStart_at: Dispatch<SetStateAction<string>>
    end_at: string
    setEnd_at: Dispatch<SetStateAction<string>>
    doctor_type: string
    setDoctor_type: Dispatch<SetStateAction<string>>
}

const useRegistrationForm = (): useRegistrationFormOutput => {
    const [name, setName] = useState<string>('')
    const [surname, setSurname] = useState<string>('')
    const [phone, setPhone] = useState<string>('')
    const [email, setEmail] = useState<string>('')
    const [password, setPassword] = useState<string>('')
    const [confirmPassword, setConfirmPassword] = useState<string>('')
    const [role, setRole] = useState<'master' | 'admin' | 'doctor' | 'pacient' | ''>('')
    const [start_at, setStart_at] = useState<string>('')
    const [end_at, setEnd_at] = useState<string>('')
    const [doctor_type, setDoctor_type] = useState<string>('')


    const resetState = (): void => {
        setName('')
        setSurname('')
        setPhone('')
        setEmail('')
        setPassword('')
        setConfirmPassword('')
        setRole('admin')
        setStart_at('')
        setEnd_at('')
    }

    return {
        //Functions
        resetState,

        //State
        name,
        setName,
        surname,
        setSurname,
        phone,
        setPhone,
        email,
        setEmail,
        password,
        setPassword,
        confirmPassword,
        setConfirmPassword,
        role,
        setRole,
        start_at,
        setStart_at,
        end_at,
        setEnd_at,
        doctor_type,
        setDoctor_type
    }
}

export default useRegistrationForm