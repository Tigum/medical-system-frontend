import React, { FunctionComponent, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import HomeScreen from '../screens/HomeScreen'
import { RouteType } from './interfaces'
import { Switch, Route, useLocation } from 'react-router-dom'
import { RootState } from '../reducers'

import AuthScreen from '../screens/AuthScreen'
import { checkUserToken } from '../reducers/Auth/AuthAction'
import Wrapper from '../components/Wrapper'
import PeopleRegistrationScreen from '../screens/PeopleRegistrationScreen'
import QueryScreen from '../screens/QueryScreen'
import ServiceStationRegistrationForm from '../screens/ServiceStationRegistrationScreen'
import ExamFormScreen from '../screens/ExamFormScreen'

const Routes: FunctionComponent = () => {
    const dispatch = useDispatch()
    const { pathname } = useLocation()
    const { user } = useSelector((state: RootState) => state.AuthReducer)
    useEffect(() => {
        let isSubscribed = true
        if (isSubscribed) {
            checkUserToken(dispatch)
        }
        return () => {
            isSubscribed = false
        }
        // eslint-disable-next-line
    }, [])

    const privateRoutes: RouteType[] = [
        { path: '/', component: <HomeScreen /> },
        { path: '/cadastrar', component: <HomeScreen /> },
        { path: '/cadastrar/pessoas', component: <PeopleRegistrationScreen /> },
        { path: '/cadastrar/local', component: <ServiceStationRegistrationForm /> },
        { path: '/cadastrar/exames', component: <ExamFormScreen /> },
        { path: '/consultar', component: <HomeScreen /> },
        { path: '/consultar/pessoas', component: <HomeScreen /> },
        { path: '/consultar/pessoas/users', component: <QueryScreen /> },
        { path: '/consultar/pessoas/pacients', component: <QueryScreen /> },
        { path: '/consultar/locais', component: <QueryScreen /> },
        { path: '/consultar/exames', component: <QueryScreen /> },
    ]

    const publicRoutes: RouteType[] = [
        { path: '/', component: <AuthScreen /> },
    ]

    const handleHeaderTitle = (): string | undefined => {
        switch (pathname) {
            case '/':
                return undefined
            case '/cadastrar':
                return 'Cadastrar'
            case '/cadastrar/pessoas':
                return 'Cadastrar Pessoas'
            case '/cadastrar/exames':
                return 'Cadastrar Exame ou Procedimento'
            case '/consultar':
                return 'Consultar'
            case '/consultar/pessoas':
                return 'Consultar Pessoas'
            case '/consultar/locais':
                return 'Consultar Locais'
            case '/consultar/exames':
                return 'Consultar Exames'
            case '/consultar/pessoas/users':
                return 'Consultar Médicos ou Funcionários'
            case '/cadastrar/local':
                return 'Cadastrar estabelecimento médico'
            default:
                return undefined
        }
    }
    return user
        ?
        <Wrapper headerTitle={handleHeaderTitle()} >
            <div style={{ backgroundColor: 'white', overflow: 'hidden' }}>
                <Switch>
                    {privateRoutes.map((route: RouteType, i: number) => <Route exact path={route.path} children={route.component} key={i} />)}
                </Switch>
            </div>
        </Wrapper>
        :
        <Switch>
            {publicRoutes.map((route: RouteType, i: number) => <Route path={route.path} children={route.component} key={i} />)}
        </Switch>
}


export default Routes
