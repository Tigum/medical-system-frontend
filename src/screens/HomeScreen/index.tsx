import React, { FunctionComponent, useEffect, useState } from 'react'
import { useLocation } from 'react-router'
import { CardListItemProps } from '../../components/CardListItem/Layout/interfaces'
import history from '../../history'
import Layout from './Layout'

const HomeScreen: FunctionComponent = () => {
    const { pathname } = useLocation()
    const [menuState, setMenuState] = useState<string>('/')
    const menuItems = (): CardListItemProps[] => {
        switch (pathname) {
            case '/':
                return homeItems
            case '/cadastrar':
                return registerItems
            case '/consultar':
                return queryItems
            case '/consultar/pessoas':
                return queryPeople
            default:
                return homeItems
        }
    }

    const homeItems: CardListItemProps[] = [
        {
            title: 'Cadastrar',
            descriptions: ['Cadastre estabelecimentos relacionados a saúde, pessoas e exames de pacientes cadastrados'],
            buttons: [
                {
                    label: 'Selecionar',
                    action: () => { history.push('/cadastrar') }
                }
            ],
            icon: 'folder-check'
        },
        {
            title: 'Consultar',
            descriptions: ['Consulte informações de locais, pessoas e exames'],
            buttons: [
                {
                    label: 'Selecionar',
                    action: () => { history.push('/consultar') }
                }
            ],
            icon: 'list-task'
        }
    ]

    const registerItems: CardListItemProps[] = [
        {
            title: 'Cadastrar Locais',
            descriptions: ['Cadastre Hospitais, Laboratórios, Postos de Saúde e Clínicas'],
            buttons: [
                {
                    label: 'Cadastrar',
                    action: () => { history.push('/cadastrar/local') }
                }
            ],
            icon: 'shop-window'
        },
        {
            title: 'Cadastrar Pessoas',
            descriptions: ['Cadastre médicos, funcionários administrativos e pacientes'],
            buttons: [
                {
                    label: 'Cadastrar',
                    action: () => { history.push('/cadastrar/pessoas')  }
                }
            ],
            icon: 'file-person'
        },
        {
            title: 'Cadastrar Exames',
            descriptions: ['Cadastre exames de pacientes cadastrados'],
            buttons: [
                {
                    label: 'Cadastrar',
                    action: () => { history.push('/cadastrar/exames')  }
                }
            ],
            icon: 'newspaper'
        }
    ]

    const queryItems: CardListItemProps[] = [
        {
            title: 'Consultar Locais',
            descriptions: ['Consulte Hospitais, Laboratórios, Postos de Saúde e Clínicas cadastrados'],
            buttons: [
                {
                    label: 'Consultar',
                    action: () => { history.push('/consultar/locais') }
                }
            ],
            icon: 'shop-window'
        },
        {
            title: 'Consultar Pessoas',
            descriptions: ['Consulte médicos, funcionários administrativos e pacientes cadastrados'],
            buttons: [
                {
                    label: 'Consultar',
                    action: () => { history.push('/consultar/pessoas')  }
                }
            ],
            icon: 'file-person'
        },
        {
            title: 'Consultar Exames',
            descriptions: ['Consulte exames de pacientes cadastrados'],
            buttons: [
                {
                    label: 'Consultar',
                    action: () => { history.push('/consultar/exames')  }
                }
            ],
            icon: 'newspaper'
        }
    ]

    const queryPeople: CardListItemProps[] = [
        {
            title: 'Consultar Pacientes',
            descriptions: ['Consulte informações de pacientes cadastrados'],
            buttons: [
                {
                    label: 'Consultar',
                    action: () => { history.push('/consultar/pessoas/pacients')   }
                }
            ],
            icon: 'file-person'
        },
        {
            title: 'Consultar Médicos ou Funcionários Administrativos',
            descriptions: ['Consulte informações de médicos ou funcionários administrativos cadastrados'],
            buttons: [
                {
                    label: 'Consultar',
                    action: () => { history.push('/consultar/pessoas/users')  }
                }
            ],
            icon: 'file-person'
        }
    ]

    useEffect(() => {
        let isSubscribed = true
        if(isSubscribed) {
            setMenuState(pathname)
        }
        return () => {
            isSubscribed = false
        }
    },[pathname])
    return <Layout menuItems={menuItems()} pathname={pathname}/>
}

export default HomeScreen