import React, { FunctionComponent } from 'react'
import CardList from '../../../components/CardList'
import useWindowSize from '../../../hooks/useWindowSize'
import { StyleOutput } from '../../AuthScreen/Layout/interfaces'
import { HomeScreenLayoutProps } from './interfaces'
import { Button } from 'react-bootstrap'
import history from '../../../history'

const Layout: FunctionComponent<HomeScreenLayoutProps> = ({ menuItems, pathname }) => {
    const { mainDiv } = styles
    return <div style={mainDiv}>
        <CardList menuItems={menuItems} />
        {pathname !== '/' && <Button variant="primary" onClick={() => history.goBack()}>Voltar</Button>}
    </div>
}

const styles: StyleOutput = {
    mainDiv: {
        width: '100%',
        backgroundColor: 'white'
    }
}

export default Layout