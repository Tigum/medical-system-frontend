import { CardListItemProps } from "../../../components/CardListItem/Layout/interfaces";

export interface HomeScreenLayoutProps {
    menuItems: CardListItemProps[]
    pathname: string
}