import React, { FunctionComponent, useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { authUser, checkIfIsFirstAccess } from '../../reducers/Auth/AuthAction'
import { UserInfo } from '../../reducers/Auth/AuthInterfaces'
import Layout from './Layout'

const AuthScreen: FunctionComponent = () => {
    const dispatch = useDispatch()
    const [isFirstAccess, setIsFirstAccess] = useState<boolean>(false)
    const [email, setEmail] = useState<string>('')
    const [password, setPassword] = useState<string>('')
    const [isLoading, setIsLoading] = useState<boolean>(false)
    const [ errorMsg, setErrorMsg ] = useState<string>('')

    const loginUser = (event: any): void => {
        event.preventDefault()
        setErrorMsg('')
        setIsLoading(true)
        authUser(dispatch, email, password, (user: UserInfo | null) => {
            if(user){
                setIsLoading(false)
                return
            }
            setEmail('')
            setPassword('')
            setErrorMsg('Credenciais inválidas. Tente Novamente')
            setIsLoading(false)
            return
        })
    }

    useEffect(() => {
        let isSubscribed = true
        if (isSubscribed) {
            checkIfIsFirstAccess((value: boolean) => { setIsFirstAccess(value) })
        }
        return () => {
            isSubscribed = false
        }

        // eslint-disable-next-line
    }, [])
    return <Layout
        isFirstAccess={isFirstAccess}
        email={email}
        setEmail={(email: string) => { setEmail(email) }}
        password={password}
        setPassword={(password: string) => { setPassword(password) }}
        loginUser={(event: any) => loginUser(event)}
        isLoading={isLoading}
        errorMsg={errorMsg}
    />
}

export default AuthScreen