import React, { FunctionComponent } from 'react'
import useWindowSize from '../../../hooks/useWindowSize'
import { AuthScreenLayoutProps, StyleOutput } from './interfaces'
import { Form, Button, Spinner } from 'react-bootstrap'
import RegisterForm from '../../../components/RegisterForm'

const Layout: FunctionComponent<AuthScreenLayoutProps> = ({ isFirstAccess, email, setEmail, password, setPassword, loginUser, isLoading, errorMsg }) => {
    const { height, width } = useWindowSize()
    const { mainDiv, formStyle, formGroupStyle, titleStyle, errorMsgStyle, buttonStyle } = styles

    const loginLayout = (
        <Form style={formStyle} onSubmit={(event: any) => loginUser(event)}>
            <h6 style={errorMsgStyle}>{errorMsg || ' '}</h6>
            <Form.Group controlId="register_login_email" style={formGroupStyle}>
                <Form.Label>E-mail</Form.Label>
                <Form.Control type="email" placeholder="Digite seu e-mail..." onChange={(event: any) => { setEmail(event.target.value) }} value={email} />
                <Form.Text className="text-muted">
                    Digite seu e-mail no qual você foi cadastrado.
                    </Form.Text>
            </Form.Group>

            <Form.Group controlId="register_login_password" style={formGroupStyle}>
                <Form.Label>Senha</Form.Label>
                <Form.Control type="password" placeholder="Digite sua senha..." onChange={(event: any) => { setPassword(event.target.value) }} value={password} />
            </Form.Group>

            <Button variant="primary" type="submit" style={buttonStyle}>
                {isLoading ? <Spinner animation="border" variant="light" size='sm' /> : 'Entrar'}
            </Button>
        </Form>
    )
    return <div style={{ height, width, ...mainDiv, overflowY: 'scroll' }}>
        <div style={{ height, width: '40%', ...mainDiv }}>
            <h2 style={titleStyle}>Sistema de Cadastro Médico</h2>
            <h6 style={{ ...titleStyle, marginBottom: 30 }}>
                {
                    isFirstAccess
                        ?
                        'Primeiro acesso! É necessário cadastrar um usuário Master para iniciar o sistema. Preencha as informações abaixo para começar.'
                        :
                        'Preencha as informações abaixo para acessar sua conta.'
                }
            </h6>
            {isFirstAccess ? <RegisterForm isFirstAccess={true} /> : loginLayout}
            <div style={{ height: 50, width: 100, backgroundColor: 'white', marginBottom: 100 }}></div>
        </div>
    </div>
}

const styles: StyleOutput = {
    mainDiv: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'column',
    },
    formStyle: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        width: '100% !important'
    },
    formGroupStyle: {
        width: '100%'
    },
    titleStyle: {
        fontWeight: 200,
        width: '100%',
        textAlign: 'center',
        marginTop: 40
    },
    errorMsgStyle: {
        color: 'red',
        fontWeight: 200
    },
    buttonStyle: {
        width: 100,
        height: 40
    }
}

export default Layout