import { CSSProperties } from "react";

export interface AuthScreenLayoutProps {
    isFirstAccess: boolean
    email: string
    setEmail: (email: string) => void
    password: string
    setPassword: (password: string) => void
    loginUser: (event: any) => void
    isLoading: boolean
    errorMsg: string
}

export interface StyleOutput {
    [key: string]: CSSProperties
}