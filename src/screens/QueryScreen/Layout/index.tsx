import React, { FunctionComponent } from 'react'
import CardList from '../../../components/CardList'
import { QueryScreenLayoutProps } from './interfaces'
import { Spinner, Button, Form } from 'react-bootstrap'
import { StyleOutput } from '../../AuthScreen/Layout/interfaces'
import useWindowSize from '../../../hooks/useWindowSize'
import { useLocation } from 'react-router'
import history from '../../../history'

const Layout: FunctionComponent<QueryScreenLayoutProps> = ({ listItems, isLoading, searchText, setSearchText, filteredResults }) => {
    const { spinnerDiv, noResultsStyle } = styles
    const { pathname } = useLocation()
    const { height } = useWindowSize()
    const defined_height = height ? height : 300
    const listIsEmpty: boolean = listItems.length < 1
    return isLoading
        ?
        <div style={{ ...spinnerDiv, height: defined_height * 0.6 }}>
            <Spinner animation="border" variant="primary" />
        </div>
        :
        <div>
            {!listIsEmpty && <Form>
                <Form.Group controlId="formBasicEmail">
                    <Form.Label>Filtrar por termo</Form.Label>
                    <Form.Control type="email" placeholder="Digite aqui" onChange={(event: any) => { setSearchText(event.target.value) }} value={searchText} />
                    <Form.Text className="text-muted">
                        Digite um termo referente a sua busca
                </Form.Text>
                </Form.Group>
            </Form>}
            {!listIsEmpty && <CardList menuItems={searchText.length > 0 ? filteredResults : listItems} />}
            {listIsEmpty && <h4 style={noResultsStyle}>Nenhum resultado encontrado</h4>}
            {pathname !== '/' && <Button variant="primary" onClick={() => history.goBack()}>Voltar</Button>}
            <div style={{ height: 50, width: 100, backgroundColor: 'white', marginBottom: 100 }}></div>
        </div>
}
const styles: StyleOutput = {
    spinnerDiv: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%'
    },
    noResultsStyle: {
        fontWeight: 200,
        width: '100%',
        textAlign: 'center',
        marginTop: 40
    },
}
export default Layout