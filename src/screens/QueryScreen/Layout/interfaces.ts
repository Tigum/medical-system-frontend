import { CardListItemProps } from "../../../components/CardListItem/Layout/interfaces";

export interface QueryScreenLayoutProps {
    listItems: CardListItemProps[]
    isLoading: boolean
    searchText: string
    setSearchText: (value: string) => void
    filteredResults: CardListItemProps[]
}