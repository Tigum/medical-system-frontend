import React, { FunctionComponent, useEffect, useState } from 'react'
import { useLocation } from 'react-router'
import { CardListItemProps } from '../../components/CardListItem/Layout/interfaces'
import { PacientInfo, UserInfo } from '../../reducers/Auth/AuthInterfaces'
import Layout from './Layout'
import useQuery from '../../hooks/useQuery'
import { ServiceStationInfo } from '../../reducers/Forms/FormsInterfaces'
import { ExamType } from '../../components/ExamForm/Layout/interfaces'

const QueryScreen: FunctionComponent = () => {
    const { pathname } = useLocation()
    const { 
        fetchUsers, 
        handleRole, 
        fetchPacients, 
        fetchPlaces, 
        formatStationType, 
        removeStation, 
        removeUser, 
        fetchExams, 
        fetchPacient, 
        removeExam, 
        fetchAddress,
        removePacient
    } = useQuery()
    const [isLoading, setIsLoading] = useState<boolean>(false)
    const [listItems, setListItems] = useState<any>([])
    const [filteredResults, setFilteredResults] = useState<CardListItemProps[]>([])
    const [searchText, setSearchText] = useState<string>('')

    const isUsersSearch: boolean = pathname === '/consultar/pessoas/users'
    const isPacientsSearch: boolean = pathname === '/consultar/pessoas/pacients'
    const isPlacesSearch: boolean = pathname === '/consultar/locais'
    const isExamsSearch: boolean = pathname === '/consultar/exames'

    const fetchList = async (pathname: string): Promise<void> => {
        setIsLoading(true)
        switch (pathname) {
            case '/consultar/pessoas/users':
                const items: UserInfo[] = await fetchUsers()
                const list: CardListItemProps[] = await convertUserToCard(items)
                setListItems(list)
                setIsLoading(false)
                return
            case '/consultar/pessoas/pacients':
                const pacients: PacientInfo[] = await fetchPacients()
                const list_pacients: CardListItemProps[] = await convertPacientToCard(pacients)
                setListItems(list_pacients)
                setIsLoading(false)
                return
            case '/consultar/locais':
                const places: ServiceStationInfo[] = await fetchPlaces()
                const list_places: CardListItemProps[] = await convertStationToCard(places)
                setListItems(list_places)
                setIsLoading(false)
                return
            case '/consultar/exames':
                const exams: ExamType[] = await fetchExams()
                const list_exams: CardListItemProps[] = await convertExamToCard(exams)
                setListItems(list_exams)
                setIsLoading(false)
                return
            default:
                setListItems([])
                setIsLoading(false)
                return
        }
    }

    const getPacientInfo = async (pacient_id: number | string): Promise<string[]> => {
        const pacient_exists = await fetchPacient(pacient_id)
        if (pacient_exists) {
            let pacientInfo: string[] = []
            pacientInfo.push(`Nome do paciente: ${pacient_exists.name} ${pacient_exists.surname}`)
            pacientInfo.push(`E-mail do paciente: ${pacient_exists.email}`)
            pacientInfo.push(`Telefone do paciente: ${pacient_exists.phone}`)
            return pacientInfo
        }
        return []
    }

    const getAddressInfo = async (address_id: number | string): Promise<string[]> => {
        const address_exists = await fetchAddress(address_id)
        if (address_exists) {
            let addressInfo: string[] = []
            addressInfo.push(` - `)
            addressInfo.push(`---Endereço---`)
            addressInfo.push(`Endereço: ${address_exists.street_name}`)
            addressInfo.push(`Complemento: ${address_exists.number}`)
            addressInfo.push(`CEP: ${address_exists.zipcode}`)
            addressInfo.push(`Cidade: ${address_exists.city}`)
            addressInfo.push(`Estado: ${address_exists.state}`)
            addressInfo.push(`País: ${address_exists.country}`)
            return addressInfo
        }
        return []
    }

    const convertExamToCard = async (exams: ExamType[]): Promise<CardListItemProps[]> => {
        const results: Promise<CardListItemProps>[] = exams.map(async (exam: ExamType): Promise<CardListItemProps> => {
            const pacientInfo = await getPacientInfo(exam && exam.pacient ? exam.pacient : '')
            return {
                title: `${exam.title} - ${exam.is_procedure ? 'Procedimento' : 'Exame'}`,
                descriptions: [`Descrição: ${exam.description}`, ...pacientInfo],
                icon: 'newspaper',
                buttons: [
                    {
                        label: 'Remover',
                        action: () => {
                            exam && exam.id && removeExam(exam.id, async (exams: ExamType[]) => {
                                const results = await convertExamToCard(exams)
                                setListItems(results)
                            })
                        }
                    }
                ]
            }
        })
        const wait_result = async (): Promise<CardListItemProps[]> => await Promise.all(results)
        return wait_result()
    }

    const convertStationToCard = async (places: ServiceStationInfo[]): Promise<CardListItemProps[]> => {
        const results: Promise<CardListItemProps>[] = places.map(async (place: ServiceStationInfo): Promise<CardListItemProps> => {
            const addressInfo = await getAddressInfo(place && place.address ? place.address : '')
            return {
                title: `${place.label} - ${formatStationType(place.station_type)}`,
                descriptions: [
                    `${place.private ? 'Estabelecimento privado' : 'Estabelecimento público'}`,
                    ...addressInfo
                ],
                icon: 'shop-window',
                buttons: [
                    {
                        label: 'Remover',
                        action: () => {
                            place && place.id && removeStation(place.id, async (places) => {
                                const results = await convertStationToCard(places)
                                setListItems(results)
                            })
                        }
                    }
                ]
            }
        })
        const wait_result = async (): Promise<CardListItemProps[]> => await Promise.all(results)
        return wait_result()
    }

    const convertUserToCard = async (users: UserInfo[]): Promise<CardListItemProps[]> => {
        const results: Promise<CardListItemProps>[] = users.map(async (user: UserInfo): Promise<CardListItemProps> => {
            const addressInfo = await getAddressInfo(user && user.address ? user.address : '')
            return {
                title: `${user.name} ${user.surname} - ${handleRole(user.role)} ${user.doctor_type && user.doctor_type !== 'N/A' ? user.doctor_type : ''}`,
                descriptions: [
                    `Início do expediente: ${user.start_at}`,
                    `Final do expediente: ${user.end_at}`,
                    user.email,
                    user.phone,
                    ...addressInfo
                ],
                icon: 'file-person',
                buttons: [
                    {
                        label: 'Remover',
                        action: () => {
                            user && user.id && removeUser(user.id, async (users: UserInfo[]) => {
                                const results = await convertUserToCard(users)
                                setListItems(results)
                            })
                        }
                    }
                ]
            }
        })
        const wait_result = async (): Promise<CardListItemProps[]> => await Promise.all(results)
        return wait_result()
    }

    const convertPacientToCard = async (pacients: PacientInfo[]): Promise<CardListItemProps[]> => {
        const results: Promise<CardListItemProps>[] = pacients.map(async (pacient: PacientInfo): Promise<CardListItemProps> => {
            const addressInfo = await getAddressInfo(pacient && pacient.address ? pacient.address : '')
            return {
                title: `${pacient.name} ${pacient.surname} - Paciente`,
                descriptions: [
                    pacient.email,
                    pacient.phone,
                    ...addressInfo
                ],
                icon: 'file-person',
                buttons: [
                    {
                        label: 'Remover',
                        action: () => {
                            pacient && pacient.id && removePacient(pacient.id, async (pacients: PacientInfo[]) => {
                                const results = await convertPacientToCard(pacients)
                                setListItems(results)
                            })
                        }
                    }
                ]
            }
        })
        const wait_result = async (): Promise<CardListItemProps[]> => await Promise.all(results)
        return wait_result()
    }

    const handleFilter = (text: string): void => {
        const result: CardListItemProps[] = listItems.filter((item: CardListItemProps) => {
            const input = text.toLowerCase()

            if (isUsersSearch || isPacientsSearch) {
                const title = item.title.toLowerCase()
                const phone = item.descriptions[item.descriptions.length - 1].toLowerCase()
                const email = item.descriptions[item.descriptions.length - 2].toLowerCase()
                return title.includes(input) || phone.includes(input) || email.includes(input)
            }

            if (isPlacesSearch) {
                const title = item.title.toLowerCase()
                return title.includes(input)
            }

            if (isExamsSearch) {
                const title = item.title.toLowerCase()
                const phone = item.descriptions[item.descriptions.length - 1].toLowerCase()
                const email = item.descriptions[item.descriptions.length - 2].toLowerCase()
                const name = item.descriptions[item.descriptions.length - 3].toLowerCase()
                return title.includes(input) || phone.includes(input) || email.includes(input) || name.includes(input)
            }

            return false
        })
        setFilteredResults(result)
    }

    useEffect(() => {
        let isSubscribed = true
        if (isSubscribed) {
            fetchList(pathname)
        }
        return () => {
            isSubscribed = false
        }
    }, [pathname])

    useEffect(() => {
        let isSubscribed = true
        if (isSubscribed) {
            if (searchText.length > 0) {
                handleFilter(searchText)
            }
        }
        return () => {
            isSubscribed = false
        }
    }, [searchText.length])

    return <Layout
        listItems={listItems}
        filteredResults={filteredResults}
        isLoading={isLoading}
        searchText={searchText}
        setSearchText={(value: string) => { setSearchText(value) }}
    />
}

export default QueryScreen