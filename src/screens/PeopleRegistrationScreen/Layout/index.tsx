import React, { FunctionComponent } from 'react'
import { Button } from 'react-bootstrap'
import RegisterForm from '../../../components/RegisterForm'
import { StyleOutput } from '../../AuthScreen/Layout/interfaces'
import { useSelector } from 'react-redux'
import { RootState } from '../../../reducers'
import history from '../../../history'

const Layout: FunctionComponent = () => {
    const { user } = useSelector((state: RootState) => state.AuthReducer)
    const { mainDiv, titleStyle } = styles

    return <div style={{ height: '100%', width: '100%', ...mainDiv }}>
            <h6 style={{ ...titleStyle, marginBottom: 30 }}>Cadastre um Médico, Funcionário Administrativo ou Paciente</h6>
            <RegisterForm isFirstAccess={false} /> 
            {user && <Button variant="primary" style={{marginTop: 20}} onClick={() => history.goBack()}>Voltar</Button>}
            <div style={{ height: 50, width: 100, backgroundColor: 'white', marginBottom: 100 }}></div>
        </div>
}

const styles: StyleOutput = {
    mainDiv: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'column',
    },
    titleStyle: {
        fontWeight: 200,
        width: '100%',
        textAlign: 'center',
        marginTop: 40
    }
}

export default Layout