import React, { FunctionComponent } from 'react'
import { Button } from 'react-bootstrap'
import ServiceStationForm from '../../../components/ServiceStationForm'
import history from '../../../history'
import { StyleOutput } from '../../AuthScreen/Layout/interfaces'

const Layout: FunctionComponent = () => {
    const { mainDiv, titleStyle } = styles

    return <div style={{ height: '100%', width: '100%', ...mainDiv }}>
        <h6 style={{ ...titleStyle, marginBottom: 30 }}>Preencha as informações abaixo para cadastrar um estabelecimento médico</h6>
        <ServiceStationForm />
        <Button variant="primary" style={{ marginTop: 20 }} onClick={() => history.goBack()}>Voltar</Button>
        <div style={{ height: 50, width: 100, backgroundColor: 'white', marginBottom: 100 }}></div>
    </div>
}

const styles: StyleOutput = {
    mainDiv: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'column',
    },
    titleStyle: {
        fontWeight: 200,
        width: '100%',
        textAlign: 'center',
        marginTop: 40
    }
}

export default Layout