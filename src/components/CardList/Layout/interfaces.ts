import { CardListItemProps } from '../../CardListItem/Layout/interfaces'

export interface CardListLayoutProps {
    menuItems: CardListItemProps[]
}

export interface CardListProps {
    menuItems: CardListItemProps[]
}