import React, { FunctionComponent } from 'react'
import { StyleOutput } from '../../../screens/AuthScreen/Layout/interfaces'
import CardListItem from '../../CardListItem'
import { CardListItemProps } from '../../CardListItem/Layout/interfaces'
import { CardListLayoutProps } from './interfaces'

const Layout: FunctionComponent<CardListLayoutProps> = ({ menuItems }) => {
    const { mainDiv } = styles
    return <div style={mainDiv}>
        {menuItems.map((item: CardListItemProps, i: number) => <CardListItem
            key={i}
            title={item.title}
            descriptions={item.descriptions}
            buttons={item.buttons}
            icon={item.icon}
        />)}
    </div>
}

const styles: StyleOutput = {
    mainDiv: {
        width: '100%'
    }
}

export default Layout