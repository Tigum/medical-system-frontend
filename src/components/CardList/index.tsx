import React, { FunctionComponent } from 'react'
import Layout from './Layout'
import { CardListProps } from './Layout/interfaces'

const CardList: FunctionComponent<CardListProps> = ({ menuItems }) => {
    return <Layout menuItems={menuItems} />
}

export default CardList