import { StationTypes } from "../../../reducers/Forms/FormsInterfaces";

export interface ServiceStationFormLayoutProps {
    isAddressForm: boolean
    errorMsg: string
    station_types: StationTypes[]
    goToAddressForm: (event: any) => void
    label: string
    setLabel: (value: string) => void
    setIsPrivate: (value: string) => void
    setStation_type: (value: StationTypes) => void
}