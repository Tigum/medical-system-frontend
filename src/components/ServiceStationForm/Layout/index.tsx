import React, { FunctionComponent } from 'react'
import { Form, Button} from 'react-bootstrap'
import { StationTypes } from '../../../reducers/Forms/FormsInterfaces'
import { StyleOutput } from '../../../screens/AuthScreen/Layout/interfaces'
import AddressForm from '../../AddressForm'
import { ServiceStationFormLayoutProps } from './interfaces'

const Layout: FunctionComponent<ServiceStationFormLayoutProps> = ({ 
    isAddressForm, 
    errorMsg, 
    station_types, 
    goToAddressForm ,
    setLabel,
    label,
    setIsPrivate,
    setStation_type
}) => {
    const { formStyle, formGroupStyle, errorMsgStyle } = styles
    const handleStationType = (station_type: string): StationTypes => {
        switch (station_type) {
            case 'Hospital':
                return 'hospital'
            case 'Laboratório':
                return 'lab'
            case 'Clínica':
                return 'clinic'
            case 'Posto de Saúde':
                return 'health_station'
            default:
                return ''
        }
    }

    const formatStationType = (station_type: StationTypes): string => {
        switch (station_type) {
            case 'hospital':
                return 'Hospital'
            case 'lab':
                return 'Laboratório'
            case 'clinic':
                return 'Clínica'
            case 'health_station':
                return 'Posto de Saúde'
            default:
                return ''
        }
    }

    const registerForm = (
        <Form style={formStyle} onSubmit={(event: any) => goToAddressForm(event)}>
            <h6 style={errorMsgStyle}>{errorMsg || ' '}</h6>
            <Form.Group controlId="register_label" style={formGroupStyle}>
                <Form.Label>Nome do estabelecimento</Form.Label>
                <Form.Control type="text" placeholder="Ex: Hospital Vera Cruz" onChange={(event: any) => setLabel(event.target.value)} value={label} />
            </Form.Group>

            <Form.Group controlId="select_private" style={formGroupStyle}>
                <Form.Label>Privado ou Público?</Form.Label>
                <Form.Control as="select" onChange={(event: any) => {
                    setIsPrivate(event.target.value)
                    }}>
                <option onClick={() => setIsPrivate('')}>{'Nenhuma'}</option>
                    {['Privado', 'Público'].map((isPrivate: string, i: number) => <option key={i}>{isPrivate}</option>)}
                </Form.Control>
            </Form.Group>

            <Form.Group controlId="select_station_type" style={formGroupStyle}>
                <Form.Label>Selecione o tipo de estabelecimento</Form.Label>
                <Form.Control as="select" onChange={(event: any) => setStation_type(handleStationType(event.target.value))}>
                    <option onClick={() => setStation_type('')}>{'Nenhuma'}</option>
                    {station_types.map((station_type: StationTypes, i: number) => station_type && <option key={i}>{formatStationType(station_type)}</option>)}
                </Form.Control>
            </Form.Group>

            <Button variant="primary" type="submit">
                Iniciar cadastro
                </Button>
        </Form>
    )

    

    return isAddressForm ? <AddressForm isFirstAccess={false} registrationType='place'/> : registerForm
}

const styles: StyleOutput = {
    formStyle: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        width: '100%'
    },
    formGroupStyle: {
        width: '100%'
    },
    errorMsgStyle: {
        color: 'red',
        fontWeight: 200
    }
}

export default Layout