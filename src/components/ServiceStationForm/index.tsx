import React, { FunctionComponent, useState } from 'react'
import { useDispatch } from 'react-redux'
import useServiceStationForm from '../../hooks/useServiceStationForm'
import { dispatchServiceStationFormData } from '../../reducers/Forms/FormsAction'
import { ServiceStationInfo, StationTypes } from '../../reducers/Forms/FormsInterfaces'
import Layout from './Layout'

const ServiceStationForm: FunctionComponent = () => {
    const dispatch = useDispatch()
    const [isAddressForm, setIsAddressForm] = useState<boolean>(false)
    const [errorMsg, setErrorMsg] = useState<string>('')
    const { label, isPrivate, station_type, setLabel, setIsPrivate, setStation_type } = useServiceStationForm()

    const formatField = (key: string): string => {
        switch (key) {
            case 'label':
                return 'Nome do estabelecimento'
            case 'private':
                return 'Privado?'
            case 'station_type':
                return 'Tipo do estabelecimento'
            default:
                return ''
        }
    }

    const goToAddressForm = (event: any): void => {
        event.preventDefault()
        setErrorMsg('')
        setIsAddressForm(false)
        const fields: any = {
            label,
            private: isPrivate,
            station_type
        }
        for (let key in fields) {
            if (!fields[key]) {
                setErrorMsg(`Favor preencher o campo ${formatField(key)}`)
                return
            }
        }

        const serviceStationData: ServiceStationInfo = {
            label,
            private: isPrivate === 'Privado',
            station_type
        }
        dispatchServiceStationFormData(dispatch, serviceStationData)
        setIsAddressForm(true)
    }

    const station_types_select: StationTypes[] = [
        'hospital',
        'lab',
        'clinic',
        'health_station'
    ]

    return <Layout
        isAddressForm={isAddressForm}
        errorMsg={errorMsg}
        station_types={station_types_select}
        goToAddressForm={goToAddressForm}
        label={label}
        setLabel={(value: string) => { setLabel(value) }}
        setIsPrivate={(value: string) => { setIsPrivate(value) }}
        setStation_type={(value: StationTypes) => { setStation_type(value) }}
    />
}

export default ServiceStationForm