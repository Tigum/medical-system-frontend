import React, { FunctionComponent } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import history from '../../history'
import { RootState } from '../../reducers'
import { RESET_AUTH_STATE } from '../../reducers/types'
import { TOKEN_KEY } from '../../utils/token_config'
import Layout from './Layout'
import { HeaderProps } from './Layout/interfaces'

const Header: FunctionComponent<HeaderProps> = ({ title }) => {
    const dispatch = useDispatch()
    const { user } = useSelector((state: RootState) => state.AuthReducer)
    const logUserOut = () => {
        history.push('/')
        localStorage.removeItem(TOKEN_KEY)
        dispatch({ type: RESET_AUTH_STATE })
    }
    return <Layout user={user} title={title} logUserOut={logUserOut} />
}

export default Header