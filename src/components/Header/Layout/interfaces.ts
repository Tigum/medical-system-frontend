import { UserInfo } from "../../../reducers/Auth/AuthInterfaces";

export interface HeaderLayoutProps {
    user: UserInfo | null
    title?: string
    logUserOut: () => void
}

export interface HeaderProps {
    title?: string
}

export interface MenuItemType {
    label: string
    action: () => void
}