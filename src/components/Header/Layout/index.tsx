import React, { FunctionComponent } from 'react'
import { StyleOutput } from '../../../screens/AuthScreen/Layout/interfaces'
import { HeaderLayoutProps, MenuItemType } from './interfaces'
import { Dropdown, DropdownButton, Button } from 'react-bootstrap'
import { HEADER_HEIGHT } from '../../../utils/theme'
import history from '../../../history'
import { useLocation } from 'react-router'

const Layout: FunctionComponent<HeaderLayoutProps> = ({ user, title, logUserOut }) => {
    const { pathname } = useLocation()
    const { mainDiv, titleStyle, buttonsDiv, backButton } = styles
    const menuOptions: MenuItemType[] = [
        {
            label: 'Home',
            action: () => history.push('/')
        },
        {
            label: 'Cadastrar',
            action: () => history.push('/cadastrar')
        },
        {
            label: 'Consultar',
            action: () => history.push('/consultar')
        },
        {
            label: 'Sair',
            action: logUserOut
        }
    ]

    return <div style={mainDiv}>
        <h4 style={titleStyle}>{user && user.name ? title ? title : `Olá, ${user.name}!` : 'Usuário'}</h4>
        <div style={buttonsDiv}>
        {pathname !== '/' && <Button style={backButton} variant="primary" onClick={() => history.goBack()}>Voltar</Button>}
        <DropdownButton id="dropdown-basic-button" title="Menu">
            {menuOptions.map((option: MenuItemType, i: number) => <Dropdown.Item key={i} onClick={option.action}>{option.label}</Dropdown.Item>)}
        </DropdownButton>
        </div>
    </div>
}

const styles: StyleOutput = {
    mainDiv: {
        width: '100%',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 20,
        marginBottom: 20,
        height: HEADER_HEIGHT
    },
    titleStyle: {
        fontWeight: 200
    },
    buttonsDiv: {
        display: 'flex'
    },
    backButton: {
        marginRight: 20
    }
}
export default Layout