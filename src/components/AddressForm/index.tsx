import React, { FunctionComponent, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import history from '../../history'
import useAddressForm from '../../hooks/useAddressForm'
import { RootState } from '../../reducers'
import { registerNewUser } from '../../reducers/Auth/AuthAction'
import { AddressType, PacientInfo, UserInfo } from '../../reducers/Auth/AuthInterfaces'
import { registerServiceStation } from '../../reducers/Forms/FormsAction'
import { ServiceStationInfo } from '../../reducers/Forms/FormsInterfaces'
import { ToasterType } from '../CustomToaster/Layout/interfaces'
import Layout from './Layout'
import { AddressFormProps, RegistrationType } from './Layout/interfaces'

const AddressForm: FunctionComponent<AddressFormProps> = ({ isFirstAccess, registrationType }) => {
    const dispatch = useDispatch()
    const { user } = useSelector((state: RootState) => state.AuthReducer)
    const { userData, serviceStationData } = useSelector((state: RootState) => state.FormsReducer)
    const [errorMsg, setErrorMsg] = useState<string>('')
    const [isLoading, setIsLoading] = useState<boolean>(false)
    const [showToaster, setShowToaster] = useState<ToasterType>({ show: false, title: '', body: '', icon: '' })
    const {
        street_name,
        setStree_name,
        zipcode,
        setZipcode,
        number,
        setNumber,
        city,
        setCity,
        state,
        setState,
        country,
        setCountry,
    } = useAddressForm()

    const formatField = (key: string): string => {
        switch (key) {
            case 'street_name':
                return 'Endereço'
            case 'zipcode':
                return 'CEP'
            case 'number':
                return 'Complemento (Opcional)'
            case 'city':
                return 'Cidade'
            case 'state':
                return 'Estado'
            case 'country':
                return 'País'
            default:
                return ''
        }
    }

    const handleRegistration = (registrationType: RegistrationType, address: AddressType): void => {
        switch (registrationType) {
            case 'people':
                const userFormData: UserInfo | PacientInfo | null = userData?.role === 'pacient'
                    ?
                    {
                        name: userData.name,
                        surname: userData.surname,
                        phone: userData.phone,
                        email: userData.email,
                        role: userData.role
                    }
                    :
                    userData

                registerNewUser(dispatch, userFormData, address, isFirstAccess, (userInfo: UserInfo | PacientInfo | null) => {
                    if (user) {
                        setShowToaster({ show: true, title: 'Cadastro de pessoas', body: `O ${userFormData?.role === 'pacient' ? 'paciente' : 'usuário'} ${userInfo?.name} ${userInfo?.surname} foi cadastrado com sucesso`, icon: 'check-circle' })
                        const interval = setTimeout(() => {
                            setShowToaster({ show: false, title: '', body: '', icon: '' })
                            setIsLoading(false)
                            clearTimeout(interval)
                            history.goBack()
                        }, 1000)
                    }
                })
                return
            case 'place':
                const serviceStationFormData: any = {
                    label: serviceStationData && serviceStationData.label ? serviceStationData.label : '',
                    private: serviceStationData?.private,
                    station_type: serviceStationData && serviceStationData.station_type ? serviceStationData.station_type : '',
                    address: serviceStationData?.address
                }
                registerServiceStation(address, serviceStationFormData, (serviceStation:ServiceStationInfo | null) => {
                    if(serviceStation){
                        setShowToaster({ show: true, title: 'Cadastro de locais', body: `${serviceStation.label} cadastrado com sucesso`, icon: 'check-circle' })
                        const interval = setTimeout(() => {
                            setShowToaster({ show: false, title: '', body: '', icon: '' })
                            setIsLoading(false)
                            clearTimeout(interval)
                            history.goBack()
                        }, 1000)
                    }
                })
                return
            case 'exam':
                return
            default:
                return
        }
    }

    const registerUser = (event: any): void => {
        event.preventDefault()
        setErrorMsg('')
        setIsLoading(true)
        const fields: any = {
            street_name,
            zipcode,
            number,
            city,
            state,
            country
        }
        for (let key in fields) {
            console.log('fields[key]', key)
            if (!fields[key] && key !== 'number') {
                console.log('fields[key] - entrou', key)
                setErrorMsg(`Favor preencher o campo ${formatField(key)}`)
                setIsLoading(false)
                return
            }
        }

        const address: AddressType = {
            street_name,
            zipcode,
            number,
            city,
            state,
            country
        }
        handleRegistration(registrationType, address)
    }

    return <Layout
        street_name={street_name}
        setStree_name={(value: string) => { setStree_name(value) }}
        zipcode={zipcode}
        setZipcode={(value: string) => { setZipcode(value) }}
        number={number}
        setNumber={(value: string) => { setNumber(value) }}
        city={city}
        setCity={(value: string) => { setCity(value) }}
        state={state}
        setState={(value: string) => { setState(value) }}
        country={country}
        setCountry={(value: string) => { setCountry(value) }}
        registerUser={(event: any) => registerUser(event)}
        errorMsg={errorMsg}
        isLoading={isLoading}
        showToaster={showToaster}
    />
}

export default AddressForm