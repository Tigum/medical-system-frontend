import React, { FunctionComponent } from 'react'
import { Form, Button, Spinner } from 'react-bootstrap'
import { StyleOutput } from '../../../screens/AuthScreen/Layout/interfaces'
import CustomToaster from '../../CustomToaster'
import { AddressFormLayoutProps } from './interfaces'

const Layout: FunctionComponent<AddressFormLayoutProps> = ({
    street_name,
    setStree_name,
    zipcode,
    setZipcode,
    number,
    setNumber,
    city,
    setCity,
    state,
    setState,
    country,
    setCountry,
    registerUser,
    errorMsg,
    isLoading,
    showToaster
}) => {
    const { formStyle, formGroupStyle, errorMsgStyle, buttonStyle } = styles
    return (
        <Form style={formStyle} onSubmit={(event: any) => registerUser(event)}>
            <h6 style={errorMsgStyle}>{errorMsg || ' '}</h6>
            <CustomToaster show={showToaster.show} title={showToaster.title} body={showToaster.body} icon={showToaster.icon} />
            <Form.Group controlId="register_street_name" style={formGroupStyle}>
                <Form.Label>Endereço</Form.Label>
                <Form.Control type="text" placeholder="Ex: Rua Almeida" onChange={(event: any) => { setStree_name(event.target.value) }} value={street_name} />
            </Form.Group>

            <Form.Group controlId="register_number" style={formGroupStyle}>
                <Form.Label>Complemento</Form.Label>
                <Form.Control type="text" placeholder="Ex: 100" onChange={(event: any) => { setNumber(event.target.value) }} value={number} />
            </Form.Group>

            <Form.Group controlId="register_city" style={formGroupStyle}>
                <Form.Label>Cidade</Form.Label>
                <Form.Control type="text" placeholder="Ex: Campinas" onChange={(event: any) => { setCity(event.target.value) }} value={city} />
            </Form.Group>

            <Form.Group controlId="register_state" style={formGroupStyle}>
                <Form.Label>Estado</Form.Label>
                <Form.Control type="text" placeholder="Ex: São Paulo" onChange={(event: any) => { setState(event.target.value) }} value={state} />
            </Form.Group>

            <Form.Group controlId="register_country" style={formGroupStyle}>
                <Form.Label>País</Form.Label>
                <Form.Control type="text" placeholder="Ex: Brasil" onChange={(event: any) => { setCountry(event.target.value) }} value={country} />
            </Form.Group>

            <Form.Group controlId="register_zipcode" style={formGroupStyle}>
                <Form.Label>CEP</Form.Label>
                <Form.Control type="text" placeholder="Ex: 13041-620" onChange={(event: any) => { setZipcode(event.target.value) }} value={zipcode} />
            </Form.Group>

            <Button style={buttonStyle} variant="primary" type="submit" disabled={isLoading}>
            {isLoading ? <Spinner animation="border" variant="light" size='sm' /> : 'Cadastrar'}
                </Button>
        </Form>
    )
}

const styles: StyleOutput = {
    formStyle: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        width: '100%'
    },
    formGroupStyle: {
        width: '100%'
    },
    errorMsgStyle: {
        color: 'red',
        fontWeight: 200
    },
    buttonStyle: {
        width: 100,
        height: 40
    }
}

export default Layout