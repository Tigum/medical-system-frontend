import { AddressType, UserInfo } from "../../../reducers/Auth/AuthInterfaces";
import { ToasterType } from "../../CustomToaster/Layout/interfaces";

export interface AddressFormLayoutProps {
    street_name: string
    setStree_name: (value: string) => void
    zipcode: string
    setZipcode: (value: string) => void
    number: string
    setNumber: (value: string) => void
    city: string
    setCity: (value: string) => void
    state: string
    setState: (value: string) => void
    country: string
    setCountry: (value: string) => void
    registerUser: (event: any) => void
    errorMsg: string
    isLoading: boolean
    showToaster: ToasterType
}

export interface AddressFormProps {
    isFirstAccess: boolean
    registrationType: RegistrationType
}

export type RegistrationType = 'people' | 'place' | 'exam'