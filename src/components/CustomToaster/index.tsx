import React, { FunctionComponent } from 'react'
import Layout from './Layout'
import { CustomToasterProps } from './Layout/interfaces'

const CustomToaster: FunctionComponent<CustomToasterProps> = ({ title, body, icon, show}) => {
    return <Layout title={title} body={body} icon={icon} show={show}/>
}

export default CustomToaster