export interface CustomToasterLayoutProps {
    show: boolean
    title: string
    body: string
    icon: string
}

export interface CustomToasterProps {
    show: boolean
    title: string
    body: string
    icon: string
}

export interface ToasterType {
    show: boolean
    title: string
    body: string
    icon: string
}