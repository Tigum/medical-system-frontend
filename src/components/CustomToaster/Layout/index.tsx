import React, { FunctionComponent, useState } from 'react'
import { Toast } from 'react-bootstrap'
import { CustomToasterLayoutProps } from './interfaces'
import { CheckCircle, Info } from 'react-bootstrap-icons'
import { StyleOutput } from '../../../screens/AuthScreen/Layout/interfaces'

const Layout: FunctionComponent<CustomToasterLayoutProps> = ({ title, body, icon, show }) => {
    const { iconStyle } = styles
    const [ close, setClose] = useState<string>('not-closed')
    const ICON_SIZE = 22
    const handleIcon = (): any => {
        switch (icon) {
            case 'check-circle':
                return <CheckCircle style={{...iconStyle, color: 'green'}} size={ICON_SIZE} />
            default:
                return <Info style={iconStyle} size={ICON_SIZE} />
        }
    }

    return <Toast
        style={{
            position: 'absolute',
            top: 10,
            right: 10,
            backgroundColor: '#017bff'
        }}
        show={close === 'not-closed' ? show : close !== 'closed' }
        onClose={() => setClose('closed')}
    >
        <Toast.Header>
            {handleIcon()}
            <strong className="mr-auto">{title}</strong>
        </Toast.Header>
        <Toast.Body style={{color: 'white'}}>{body}</Toast.Body>
    </Toast>
}

const styles: StyleOutput = {
    iconStyle: {
        marginRight: 10
    }
}
export default Layout