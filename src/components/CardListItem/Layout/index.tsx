import React, { FunctionComponent } from 'react'
import { Card, Button } from 'react-bootstrap'
import { StyleOutput } from '../../../screens/AuthScreen/Layout/interfaces'
import { ButtonType, CardListItemLayoutProps } from './interfaces'
import { FolderCheck, Info, ListTask, ShopWindow, FilePerson, Newspaper } from 'react-bootstrap-icons'

const Layout: FunctionComponent<CardListItemLayoutProps> = ({ title, descriptions, buttons, icon }) => {
    const { cardDiv, iconStyle, buttonStyle } = styles
    const ICON_SIZE = 30
    const handleIcons = (icon: string): any => {
        switch (icon) {
            case 'folder-check':
                return <FolderCheck style={iconStyle} size={ICON_SIZE} />
            case 'list-task':
                return <ListTask style={iconStyle} size={ICON_SIZE} />
            case 'shop-window':
                return <ShopWindow style={iconStyle} size={ICON_SIZE} />
            case 'file-person':
                return <FilePerson style={iconStyle} size={ICON_SIZE} />
            case 'newspaper':
                return <Newspaper style={iconStyle} size={ICON_SIZE} />
            default:
                return <Info style={iconStyle} size={ICON_SIZE} />
        }
    }
    return (
        <Card style={cardDiv}>
            <Card.Body>
                <Card.Title>{handleIcons(icon)} {title}</Card.Title>
                 {descriptions.map((description: string, i: number) => <Card.Text key={i}>{description}</Card.Text>)}
                {buttons.map((button: ButtonType, i: number)=> <Button key={i} style={buttonStyle} variant="primary" onClick={button.action}>{button.label}</Button>)}
            </Card.Body>
        </Card>
    )
}

const styles: StyleOutput = {
    cardDiv: {
        width: '100%',
        marginBottom: 20
    },
    iconStyle: {
        marginRight: 10
    },
    buttonStyle: {
        marginRight: 10
    }
}

export default Layout