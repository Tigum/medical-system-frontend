export interface CardListItemLayoutProps {
    title: string
    descriptions: string[]
    buttons: ButtonType[]
    icon: string
}

export interface CardListItemProps {
    title: string
    descriptions: string[]
    buttons: ButtonType[]
    icon: string
}

export interface ButtonType {
    label: string
    action: () => void
}