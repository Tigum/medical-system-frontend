import React, { FunctionComponent } from 'react'
import Layout from './Layout'
import { CardListItemProps } from './Layout/interfaces'

const CardListItem: FunctionComponent<CardListItemProps> = ({ title, descriptions, buttons, icon}) => {
    return <Layout title={title} descriptions={descriptions} buttons={buttons} icon={icon}/>
}

export default CardListItem