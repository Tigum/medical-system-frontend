import React, { FunctionComponent } from 'react'
import { Form, Button, Spinner } from 'react-bootstrap'
import { PacientInfo } from '../../../reducers/Auth/AuthInterfaces'
import { StyleOutput } from '../../../screens/AuthScreen/Layout/interfaces'
import CustomToaster from '../../CustomToaster'
import { ExamFormLayoutProps, ProcedureOrExam } from './interfaces'

const Layout: FunctionComponent<ExamFormLayoutProps> = ({
    errorMsg,
    title,
    setTitle,
    description,
    setDescription,
    setIs_procedure,
    setSelectedPacient,
    handleProcedureOrExameLabel,
    formatProcedureOrExameLabel,
    pacients,
    showToaster,
    isLoading,
    register
}) => {
    const { formStyle, formGroupStyle, errorMsgStyle } = styles

    const procedure_or_exam: ProcedureOrExam[] = ['exam', 'procedure']

    const registerForm = (
        <Form style={formStyle} onSubmit={(event: any) => { register(event) }}>
            <h6 style={errorMsgStyle}>{errorMsg || ' '}</h6>
            <CustomToaster show={showToaster.show} title={showToaster.title} body={showToaster.body} icon={showToaster.icon} />
            <Form.Group controlId="select_procedure_or_exam" style={formGroupStyle}>
                <Form.Label>Selecione exame ou procedimento</Form.Label>
                <Form.Control as="select" onChange={(event: any) => setIs_procedure(formatProcedureOrExameLabel(event.target.value))}>
                    <option onClick={() => setIs_procedure('')}>{'Selecionar'}</option>
                    {procedure_or_exam.map((item: ProcedureOrExam, i: number) => item && <option key={i}>{handleProcedureOrExameLabel(item)}</option>)}
                </Form.Control>
            </Form.Group>

            <Form.Group controlId="register_exam_title" style={formGroupStyle}>
                <Form.Label>Título</Form.Label>
                <Form.Control type="text" placeholder="Ex: Hemograma completo" onChange={(event: any) => setTitle(event.target.value)} value={title} />
            </Form.Group>

            <Form.Group controlId="register_exam_description" style={formGroupStyle}>
                <Form.Label>Descrição</Form.Label>
                <Form.Control type="text" placeholder="Ex: O exame de sangue foi..." onChange={(event: any) => setDescription(event.target.value)} value={description} />
            </Form.Group>

            <Form.Group controlId="select_pacient" style={formGroupStyle}>
                <Form.Label>Selecione o paciente</Form.Label>
                <Form.Control as="select" onChange={(event: any) => {
                    setSelectedPacient(parseFloat(event.target.value))
                    }}>
                    <option onClick={() => {
                        setSelectedPacient(null)
                    }}>{'Nenhuma'}</option>
                    {pacients.map((pacient: PacientInfo, i: number) => pacient && <option key={i} value={pacient.id}>{`${pacient.name} ${pacient.surname}`}</option>)}
                </Form.Control>
            </Form.Group>

            <Button variant="primary" type="submit">
                {isLoading ? <Spinner animation="border" variant="light" size='sm' /> : 'Cadastrar'}
            </Button>
        </Form>
    )

    return registerForm
}

const styles: StyleOutput = {
    formStyle: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        width: '100%'
    },
    formGroupStyle: {
        width: '100%'
    },
    errorMsgStyle: {
        color: 'red',
        fontWeight: 200
    }
}

export default Layout