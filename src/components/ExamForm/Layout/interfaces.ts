import { PacientInfo } from "../../../reducers/Auth/AuthInterfaces";
import { ToasterType } from "../../CustomToaster/Layout/interfaces";

export interface ExamFormLayoutProps {
    errorMsg: string
    title: string
    setTitle: (title: string) => void
    description: string
    setDescription: (description: string) => void
    setIs_procedure: (value: ProcedureOrExam) => void
    setSelectedPacient: (pacient: number | null) => void
    handleProcedureOrExameLabel: (value: ProcedureOrExam) => string
    formatProcedureOrExameLabel: (value: string) => ProcedureOrExam
    pacients: PacientInfo[]
    showToaster: ToasterType
    isLoading: boolean
    register: (event: any) => void
}

export interface ExamType {
    id?: number
    title: string
    description: string
    is_procedure: boolean
    pacient?: number | string
}

export type ProcedureOrExam = 'exam' | 'procedure' | ''