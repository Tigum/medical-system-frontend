import React, { FunctionComponent, useEffect, useState } from 'react'
import history from '../../history'
import useExamForm from '../../hooks/useExamForm'
import useQuery from '../../hooks/useQuery'
import { PacientInfo } from '../../reducers/Auth/AuthInterfaces'
import { ToasterType } from '../CustomToaster/Layout/interfaces'
import Layout from './Layout'
import { ExamType, ProcedureOrExam } from './Layout/interfaces'

const ExamForm: FunctionComponent = () => {
    const [errorMsg, setErrorMsg] = useState<string>('')
    const [pacients, setPacients] = useState<PacientInfo[]>([])
    const [showToaster, setShowToaster] = useState<ToasterType>({ show: false, title: '', body: '', icon: '' })
    const [isLoading, setIsLoading] = useState<boolean>(false)
    const { fetchPacients } = useQuery()
    const {
        title,
        setTitle,
        description,
        setDescription,
        is_procedure,
        setIs_procedure,
        selectedPacient,
        setSelectedPacient,
        handleProcedureOrExameLabel,
        formatProcedureOrExameLabel,
        registerExam
    } = useExamForm()

    const getPacients = async (): Promise<void> => {
        const result = await fetchPacients()
        setPacients(result)
    }

    const formatField = (key: string): string => {
        switch (key) {
            case 'is_procedure':
                return 'Selecione exame ou procedimento'
            case 'title':
                return 'Título'
            case 'description':
                return 'Descrição'
            case 'selectedPacient':
                return 'Selecione o paciente'
            default:
                return ''
        }
    }

    const register = (event: any): void => {
        event.preventDefault()
        setIsLoading(true)
        setErrorMsg('')
        const fields: any = {
            is_procedure,
            title,
            description,
            selectedPacient
        }

        for (let key in fields) {
            if (!fields[key]) {
                setIsLoading(false)
                setErrorMsg(`Favor preencher o campo ${formatField(key)}`)
                return
            }
        }
        
        const examFormData: ExamType = {
            title,
            description,
            is_procedure: is_procedure === 'procedure',
            pacient: selectedPacient ? selectedPacient : -1
        }
        
        registerExam(examFormData, (exam: ExamType) => {
            if (exam) {
                setShowToaster({
                    show: true,
                    title: `Cadastro de ${exam.is_procedure ? 'procedimento' : 'exame'}`,
                    body: `O ${exam.is_procedure ? 'procedimento' : 'exame'} ${exam.title} foi cadastrado com sucesso`,
                    icon: 'check-circle'
                })
                const interval = setTimeout(() => {
                    setShowToaster({ show: false, title: '', body: '', icon: '' })
                    setIsLoading(false)
                    clearTimeout(interval)
                    history.goBack()
                }, 1000)
            }
        })

    }

    useEffect(() => {
        let isSubscribed = true

        if (isSubscribed) {
            getPacients()
        }

        return () => {
            isSubscribed = false
        }
        // eslint-disable-next-line
    }, [])

    return <Layout
        errorMsg={errorMsg}
        title={title}
        setTitle={(title: string) => { setTitle(title) }}
        description={description}
        setDescription={(description: string) => { setDescription(description) }}
        setIs_procedure={(value: ProcedureOrExam) => { setIs_procedure(value) }}
        setSelectedPacient={(pacient: number | null) => { setSelectedPacient(pacient) }}
        handleProcedureOrExameLabel={(value: ProcedureOrExam) => handleProcedureOrExameLabel(value)}
        formatProcedureOrExameLabel={(value: string) => formatProcedureOrExameLabel(value)}
        pacients={pacients}
        showToaster={showToaster}
        isLoading={isLoading}
        register={(event: any) => register(event)}
    />
}

export default ExamForm