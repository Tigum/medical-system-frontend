import React, { FunctionComponent } from 'react'
import { Form, Button } from 'react-bootstrap'
import { StyleOutput } from '../../../screens/AuthScreen/Layout/interfaces'
import AddressForm from '../../AddressForm'
import { PermissionType, RegisterFormLayoutProps } from './interfaces'

const Layout: FunctionComponent<RegisterFormLayoutProps> = ({
    isAddressForm,
    goToAddressForm,
    errorMsg,
    name,
    setName,
    surname,
    setSurname,
    phone,
    setPhone,
    email,
    setEmail,
    password,
    setPassword,
    confirmPassword,
    setConfirmPassword,
    setRole,
    start_at,
    setStart_at,
    end_at,
    setEnd_at,
    permissions,
    formatPermission,
    isFirstAccess,
    isPacient,
    isDoctor,
    doctor_type,
    setDoctor_type
}) => {
    const { formStyle, formGroupStyle, errorMsgStyle } = styles
    const registerForm = (
        <Form style={formStyle} onSubmit={(event: any) => goToAddressForm(event)}>
            <h6 style={errorMsgStyle}>{errorMsg || ' '}</h6>
            <Form.Group controlId="register_name" style={formGroupStyle}>
                <Form.Label>Nome</Form.Label>
                <Form.Control type="text" placeholder="Ex: João" onChange={(event: any) => setName(event.target.value)} value={name} />
            </Form.Group>

            <Form.Group controlId="register_surname" style={formGroupStyle}>
                <Form.Label>Sobrenome</Form.Label>
                <Form.Control type="text" placeholder="Ex: Silva" onChange={(event: any) => setSurname(event.target.value)} value={surname} />
            </Form.Group>

            <Form.Group controlId="register_phone" style={formGroupStyle}>
                <Form.Label>Telefone</Form.Label>
                <Form.Control type="text" placeholder="Ex: (00) 00000-0000" onChange={(event: any) => setPhone(event.target.value)} value={phone} />
            </Form.Group>

            <Form.Group controlId="register_email" style={formGroupStyle}>
                <Form.Label>E-mail</Form.Label>
                <Form.Control type="email" placeholder="Ex: email@gmail.com" onChange={(event: any) => setEmail(event.target.value)} value={email} />
                <Form.Text className="text-muted">
                    Favor digitar um e-mail válido.
                    </Form.Text>
            </Form.Group>

            <Form.Group controlId="select_permission" style={formGroupStyle}>
                <Form.Label>Selecione a permissão</Form.Label>
                <Form.Control as="select" onChange={(event: any) => setRole(handleRole(event.target.value))}>
                    {!isFirstAccess && <option onClick={() => setRole('')}>{'Nenhuma'}</option>}
                    {permissions.map((permission: PermissionType, i: number) => permission && <option key={i}>{formatPermission(permission)}</option>)}
                </Form.Control>
            </Form.Group>

            {isDoctor && <Form.Group controlId="register_doctor_type" style={formGroupStyle}>
                <Form.Label>Especialidade médica</Form.Label>
                <Form.Control type="text" placeholder="Ex: Pediatra" onChange={(event: any) => setDoctor_type(event.target.value)} value={doctor_type} />
            </Form.Group>}


            {!isPacient && <Form.Group controlId="register_start_at" style={formGroupStyle}>
                <Form.Label>Início do expediente</Form.Label>
                <Form.Control type="text" placeholder="Ex: 09:00" onChange={(event: any) => setStart_at(event.target.value)} value={start_at} />
            </Form.Group>}

            {!isPacient && <Form.Group controlId="register_end_at" style={formGroupStyle}>
                <Form.Label>Final do expediente</Form.Label>
                <Form.Control type="text" placeholder="Ex: 18:00" onChange={(event: any) => setEnd_at(event.target.value)} value={end_at} />
            </Form.Group>}

            {!isPacient && <Form.Group controlId="register_password" style={formGroupStyle}>
                <Form.Label>Senha</Form.Label>
                <Form.Control type="password" placeholder="Digite sua senha..." onChange={(event: any) => setPassword(event.target.value)} value={password} />
            </Form.Group>}

            {!isPacient && <Form.Group controlId="register_confirm_password" style={formGroupStyle}>
                <Form.Label>Confirme sua senha</Form.Label>
                <Form.Control type="password" placeholder="Digite sua senha novamente..." onChange={(event: any) => setConfirmPassword(event.target.value)} value={confirmPassword} />
            </Form.Group>}
            <Button variant="primary" type="submit">
                Iniciar cadastro
                </Button>
        </Form>
    )
    const handleRole = (role: string): 'master' | 'admin' | 'doctor' | 'pacient' | '' => {
        switch (role) {
            case 'Médico':
                return 'doctor'
            case 'Administrador':
                return 'admin'
            case 'Master':
                return 'master'
            case 'Paciente':
                return 'pacient'
            default:
                return 'admin'
        }
    }
    return isAddressForm ? <AddressForm isFirstAccess={isFirstAccess} registrationType='people'/> : registerForm
}

const styles: StyleOutput = {
    formStyle: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        width: '100%'
    },
    formGroupStyle: {
        width: '100%'
    },
    errorMsgStyle: {
        color: 'red',
        fontWeight: 200
    }
}

export default Layout