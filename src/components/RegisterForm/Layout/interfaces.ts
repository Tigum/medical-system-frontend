export interface RegisterFormLayoutProps {
    isAddressForm: boolean
    goToAddressForm: (event: any) => void
    errorMsg: string
    name: string
    setName: (value: string) => void
    surname: string
    setSurname: (value: string) => void
    phone: string
    setPhone: (value: string) => void
    email: string
    setEmail: (value: string) => void
    password: string
    setPassword: (value: string) => void
    confirmPassword: string
    setConfirmPassword: (value: string) => void
    setRole: (value: 'master' | 'admin' | 'doctor' | 'pacient' | '') => void
    start_at: string
    setStart_at: (value: string) => void
    end_at: string
    setEnd_at: (value: string) => void
    permissions: PermissionType[]
    formatPermission: (permission: PermissionType) => string
    isFirstAccess: boolean
    isPacient: boolean
    isDoctor: boolean
    doctor_type: string
    setDoctor_type: (value: string) => void
}

export interface RegisterFormProps {
    isFirstAccess: boolean
}

export type PermissionType = 'master' | 'admin' | 'doctor' | 'pacient' | ''