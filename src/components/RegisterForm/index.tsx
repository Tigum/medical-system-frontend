import React, { FunctionComponent, useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import useRegistrationForm from '../../hooks/useRegistrationForm'
import { UserInfo } from '../../reducers/Auth/AuthInterfaces'
import { dispatchUserFormData } from '../../reducers/Forms/FormsAction'
import Layout from './Layout'
import { PermissionType, RegisterFormProps } from './Layout/interfaces'

const RegisterForm: FunctionComponent<RegisterFormProps> = ({ isFirstAccess = false }) => {
    const dispatch = useDispatch()
    const [isAddressForm, setIsAddressForm] = useState<boolean>(false)
    const [errorMsg, setErrorMsg] = useState<string>('')
    const [isPacient, setIsPacient] = useState<boolean>(false)
    const [isDoctor, setIsDoctor] = useState<boolean>(false)
    const {
        name,
        setName,
        surname,
        setSurname,
        phone,
        setPhone,
        email,
        setEmail,
        password,
        setPassword,
        confirmPassword,
        setConfirmPassword,
        role,
        setRole,
        start_at,
        setStart_at,
        end_at,
        setEnd_at,
        doctor_type,
        setDoctor_type
    } = useRegistrationForm()

    const permissions: PermissionType[] = isFirstAccess
        ?
        [
            'master',
        ]
        : [
            'master',
            'admin',
            'doctor',
            'pacient',
            ''
        ]

    const formatPermission = (permission: PermissionType) => {
        switch (permission) {
            case 'master':
                return 'Master'
            case 'admin':
                return 'Administrador'
            case 'doctor':
                return 'Médico'
            case 'pacient':
                return 'Paciente'
            default:
                return 'Nenhuma'
        }
    }

    const formatField = (key: string): string => {
        switch (key) {
            case 'name':
                return 'Nome'
            case 'surname':
                return 'Sobrenome'
            case 'phone':
                return 'Telefone'
            case 'email':
                return 'E-mail'
            case 'password':
                return 'Senha'
            case 'role':
                return 'Permissão'
            case 'start_at':
                return 'Início do expediente'
            case 'end_at':
                return 'Final do expediente'
            case 'doctor_type':
                return 'Especialidade médica'
            default:
                return ''
        }
    }

    const goToAddressForm = (event: any): void => {
        event.preventDefault()
        setErrorMsg('')
        setIsAddressForm(false)
        const fields: any = isPacient
            ?
            {
                name,
                surname,
                phone,
                email,
                role,
            }
            :
            isDoctor
                ?
                {
                    name,
                    surname,
                    phone,
                    email,
                    password,
                    role,
                    start_at,
                    end_at,
                    doctor_type
                }
                :
                {
                    name,
                    surname,
                    phone,
                    email,
                    password,
                    role,
                    start_at,
                    end_at
                }
        for (let key in fields) {
            if (!fields[key]) {
                setErrorMsg(`Favor preencher o campo ${formatField(key)}`)
                return
            }
        }

        if (password !== confirmPassword) {
            setErrorMsg('Confirmação da senha está incorreta. Tente novamente!')
            return
        }
        const userData: UserInfo = {
            name,
            surname,
            email,
            password,
            phone,
            role,
            start_at,
            end_at,
            doctor_type: doctor_type || 'N/A'
        }
        dispatchUserFormData(dispatch, userData)
        setIsAddressForm(true)
    }
    useEffect(() => {
        let isSubscribed = true
        if (isSubscribed && isFirstAccess) {
            setRole('master')
        }
    }, [])

    useEffect(() => {
        let isSubscribed = true
        if (isSubscribed) {
            setIsPacient(role === 'pacient')
            setIsDoctor(role === 'doctor')
        }
    }, [role])

    return <Layout
        isAddressForm={isAddressForm}
        goToAddressForm={(event: any) => goToAddressForm(event)}
        errorMsg={errorMsg}
        name={name}
        setName={(value: string) => { setName(value) }}
        surname={surname}
        setSurname={(value: string) => { setSurname(value) }}
        phone={phone}
        setPhone={(value: string) => { setPhone(value) }}
        email={email}
        setEmail={(value: string) => { setEmail(value) }}
        password={password}
        setPassword={(value: string) => { setPassword(value) }}
        confirmPassword={confirmPassword}
        setConfirmPassword={(value: string) => { setConfirmPassword(value) }}
        setRole={(value: 'master' | 'admin' | 'doctor' | 'pacient' | '') => { setRole(value) }}
        start_at={start_at}
        setStart_at={(value: string) => { setStart_at(value) }}
        end_at={end_at}
        setEnd_at={(value: string) => { setEnd_at(value) }}
        permissions={permissions}
        formatPermission={formatPermission}
        isFirstAccess={isFirstAccess}
        isPacient={isPacient}
        isDoctor={isDoctor}
        doctor_type={doctor_type}
        setDoctor_type={(value: string) => { setDoctor_type(value) }}
    />
}

export default RegisterForm