import React, { FunctionComponent } from 'react'
import Layout from './Layout'
import { WrapperProps } from './Layout/interfaces'

const Wrapper: FunctionComponent<WrapperProps> = ({ children, headerTitle }) => {
    return <Layout children={children} headerTitle={headerTitle}/>
}

export default Wrapper