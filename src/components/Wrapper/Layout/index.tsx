import React, { FunctionComponent } from 'react'
import Header from '../../../components/Header'
import useWindowSize from '../../../hooks/useWindowSize'
import { StyleOutput } from '../../../screens/AuthScreen/Layout/interfaces'
import { HEADER_HEIGHT } from '../../../utils/theme'
import { WrapperLayoutProps } from './interfaces'

const Layout: FunctionComponent<WrapperLayoutProps> = ({ children, headerTitle }) => {
    const { wrapper, mainDiv } = styles
    const { height, width } = useWindowSize()
    const defined_height = height ? height : 0
    return (
        <div style={{ width, height, ...wrapper }}>
            <div style={{ ...mainDiv, height }}>
                <Header title={headerTitle} />
                <div style={{ width: '100%', height: defined_height - HEADER_HEIGHT - 40, backgroundColor: 'white' }}>
                    {children}
                </div>
            </div>
        </div>
    )
}

const styles: StyleOutput = {
    wrapper: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    mainDiv: {
        width: '60%',
        backgroundColor: 'transparent'
    }
}

export default Layout