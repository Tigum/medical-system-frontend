// Login actions
export const SET_USER_INFO = 'set_user_info'
export const SET_IS_LOGGING_IN = 'set_is_logging_in'
export const RESET_AUTH_STATE = 'reset_auth_state'

//Form actions
export const SET_USER_DATA_FORM = 'set_user_data_form'
export const SET_SERVICE_STATION_DATA_FORM = 'set_service_station_data_form'