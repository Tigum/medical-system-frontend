import { combineReducers } from "redux"
import AuthReducer from './Auth/AuthReducer'
import FormsReducer from './Forms/FormsReducer'

export const appReducer = combineReducers({
    AuthReducer,
    FormsReducer
})
export default appReducer
export type RootState = ReturnType<typeof appReducer>