import { UserInfo } from '../Auth/AuthInterfaces'
import { SET_SERVICE_STATION_DATA_FORM, SET_USER_DATA_FORM } from '../types'

export interface FormState {
    userData: UserInfo | null
    serviceStationData: ServiceStationInfo | null
}

export interface SetUserDataForm {
    type: typeof SET_USER_DATA_FORM,
    payload: UserInfo | null,
}

export interface SetServiceStationForm {
    type: typeof SET_SERVICE_STATION_DATA_FORM,
    payload: ServiceStationInfo | null,
}

export interface ServiceStationInfo {
    label: string
    private: boolean
    station_type: StationTypes
    address?: number
    id?: number
}
export type StationTypes = 'hospital' | 'lab' | 'health_station' | 'clinic' | ''


export type FormActions = SetUserDataForm | SetServiceStationForm
