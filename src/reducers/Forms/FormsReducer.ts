import { SET_SERVICE_STATION_DATA_FORM, SET_USER_DATA_FORM } from '../types'
import { FormActions, FormState } from './FormsInterfaces'

const INITIAL_STATE: FormState = {
    userData: null,
    serviceStationData: null
}

export default (state = INITIAL_STATE, action: FormActions): FormState => {
    switch (action.type) {
        case SET_USER_DATA_FORM:
            return { ...state, userData: action.payload }
        case SET_SERVICE_STATION_DATA_FORM:
            return { ...state, serviceStationData: action.payload }
        default:
            return state
    }
}
