import { AddressType, UserInfo } from "../Auth/AuthInterfaces";
import { SET_SERVICE_STATION_DATA_FORM, SET_USER_DATA_FORM } from "../types";
import { ServiceStationInfo } from "./FormsInterfaces";
import axios from 'axios'
import { API_URL } from "../../utils/api_config";

export const dispatchUserFormData = (dispatch: any, userData: UserInfo): void => {
    dispatch({
        type: SET_USER_DATA_FORM,
        payload: userData
    })
}

export const dispatchServiceStationFormData = (dispatch: any, serviceStationData: ServiceStationInfo): void => {
    dispatch({
        type: SET_SERVICE_STATION_DATA_FORM,
        payload: serviceStationData
    })
}

export const registerServiceStation = async (address: AddressType, serviceStationData: ServiceStationInfo, callback: (serviceStation: ServiceStationInfo | null) => void): Promise<ServiceStationInfo | null> =>{
    try {
        const address_result = await axios.post(`${API_URL}/address/`, address)
        if (address_result.status === 201) {
            serviceStationData['address'] = address_result.data.data.id
            const result = await axios.post(`${API_URL}/service_station/`, serviceStationData)
            if (result.status === 201) {
                const serviceStation: ServiceStationInfo = result.data.data
                callback(serviceStation)
                return serviceStation
            }
            callback(null)
            return null
        }
        callback(null)
        return null
    } catch (err) {
        console.log('ERROR REGISTERING NEW SERVICE STATION ==> ', err)
        callback(null)
        return null
    }
}