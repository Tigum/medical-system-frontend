import { SET_USER_INFO, SET_IS_LOGGING_IN, RESET_AUTH_STATE } from '../types'
import { AuthActions, AuthState } from './AuthInterfaces'

const INITIAL_STATE: AuthState = {
    user: null,
    isLoggingIn: false
}

export default (state = INITIAL_STATE, action: AuthActions): AuthState => {
    switch (action.type) {
        case SET_USER_INFO:
            return { ...state, user: action.payload }
        case SET_IS_LOGGING_IN:
            return { ...state, isLoggingIn: action.payload }
        case RESET_AUTH_STATE:
            return { ...state, ...INITIAL_STATE }
        default:
            return state
    }
}
