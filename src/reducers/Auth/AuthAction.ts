import axios from 'axios'
import { API_URL } from '../../utils/api_config'
import { TOKEN_KEY } from '../../utils/token_config'
import { SET_USER_INFO } from '../types'
import { AddressType, PacientInfo, UserInfo } from './AuthInterfaces'

export const registerNewUser = async (dispatch: any, userData: UserInfo | PacientInfo | null, address: AddressType, isFirstAccess: boolean, callback:(user: UserInfo | PacientInfo | null) => void): Promise<UserInfo | PacientInfo | null> => {
    if(!userData) return null

    if(userData.role === 'pacient'){
        try {
            const address_result = await axios.post(`${API_URL}/address/`, address)
            if (address_result.status === 201) {
                userData['address'] = address_result.data.data.id
                delete userData['role']
                const result = await axios.post(`${API_URL}/pacient/`, userData)
                if (result.status === 201) {
                    const pacient: PacientInfo = result.data.data
                    callback(pacient)
                    return pacient
                }
                callback(null)
                return null
            }
            callback(null)
            return null
        } catch (err) {
            console.log('ERROR REGISTERING NEW PACIENT ==> ', err)
            callback(null)
            return null
        }
    }

    try {
        const address_result = await axios.post(`${API_URL}/address/`, address)
        if (address_result.status === 201) {
            userData['address'] = address_result.data.data.id
            const result = await axios.post(`${API_URL}/user/`, userData)
            if (result.status === 201) {
                const user: UserInfo = result.data.data
                if(user && user.token && isFirstAccess){
                    localStorage.setItem(TOKEN_KEY, user.token )
                    dispatchUserInfo(dispatch, user)
                }
                callback(user)
                return user
            }
            callback(null)
            return null
        }
        callback(null)
        return null
    } catch (err) {
        console.log('ERROR REGISTERING NEW USER ==> ', err)
        callback(null)
        return null
    }
}

export const authUser = async (dispatch: any, email: string, password: string, callback: (user: UserInfo | null) => void): Promise<UserInfo | null> => {
    try {
        const result = await axios.post(`${API_URL}/user/login`, { email, password })
        if (result.status === 200) {
            const user: UserInfo = result.data.data
            dispatchUserInfo(dispatch, user)
            callback(user)
            if(user && user.token){
                localStorage.setItem(TOKEN_KEY, user.token )
            }
            return user
        }
        callback(null)
        return null
    } catch (err) {
        console.log('ERROR SIGNING USER IN  ==> ', err)
        callback(null)
        return null
    }
}


export const checkUserToken = async (dispatch: any): Promise<void> => {
    try {
        const token: string | null = localStorage.getItem(TOKEN_KEY)
        const result = await axios.post(`${API_URL}/user/token`, { token })
        if (result.status === 200) {
            const user = result.data.data
            dispatchUserInfo(dispatch, user)
            return
        }
        return
    } catch (err) {
        console.log('ERROR CHECKING USER TOKEN ==> ', err)
        dispatchUserInfo(dispatch, null)
        return
    }
}

export const checkIfIsFirstAccess = async (callback: (value: boolean) => void): Promise<void> => {
    try {
        const result = await axios.get(`${API_URL}/user/`)
        if (result.status === 200) {
            callback(result.data.data < 1)
            return
        }
        callback(true)
        return
    } catch (err) {
        console.log('ERROR CHECKING FIRST ACCESS ==> ', err)
        callback(true)
        return
    }
}


export const dispatchUserInfo = (dispatch: any, payload: UserInfo | null): void => {
    dispatch({
        type: SET_USER_INFO,
        payload
    })
    if (payload === null) {
        localStorage.removeItem('user_token_tcc')
    }
}
