import { SET_USER_INFO, SET_IS_LOGGING_IN, RESET_AUTH_STATE } from '../types'

export interface AuthState {
    user: null | UserInfo,
    isLoggingIn: boolean
}

export interface UserInfo {
    id?: number
    name: string
    surname: string
    email: string
    password: string
    phone: string
    role: 'master' | 'admin' | 'doctor' | 'pacient' | ''
    start_at: string
    end_at: string
    address?: number
    token?: string
    doctor_type?: string
}

export interface AddressType {
    street_name: string
    zipcode: string
    number: string
    city: string
    state: string
    country: string
}

export interface PacientInfo {
    id?: number
    name: string
    surname: string
    email: string
    phone: string
    address?: number
    role?: 'master' | 'admin' | 'doctor' | 'pacient' | ''
}

export interface SetUserInfo {
    type: typeof SET_USER_INFO,
    payload: UserInfo | null,
}

export interface SetIsLoggingIn {
    type: typeof SET_IS_LOGGING_IN,
    payload: boolean,
}

export interface ResetAuthState {
    type: typeof RESET_AUTH_STATE,
}

export type AuthActions = SetUserInfo | SetIsLoggingIn | ResetAuthState
