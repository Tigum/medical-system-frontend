import React from 'react';
import './App.css';
import Routes from './routes'
import { Router } from 'react-router-dom'
import history from './history'
import { Provider } from 'react-redux'
import store from './store'
import 'bootstrap/dist/css/bootstrap.min.css';
import useApiInterceptors from './hooks/useInterceptors';

const App = () => {
  const { ConfigApiRequests } = useApiInterceptors()
  ConfigApiRequests()
  return (
    <Provider store={store}>
      <Router history={history}>
        <Routes />
      </Router>
    </Provider>
  )
}

export default App;
